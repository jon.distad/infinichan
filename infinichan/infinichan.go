package infinichan

import (
	"gitlab.com/catharinejm/infinichan/infqueue"
)

type chan_ struct {
	in    chan interface{}
	out   chan interface{}
	side  chan msg
	queue infqueue.Queue
}

type msg uint32

const (
	starving msg = iota
	shutdown
)

func MakeChan() (chan<- interface{}, <-chan interface{}) {
	in := make(chan interface{})
	out := make(chan interface{})
	side := make(chan msg)
	queue := infqueue.NewQueue()
	go readIn(queue, side, in)
	go writeOut(queue, side, out)
	return in, out
}

func readIn(queue infqueue.Queue, side chan msg, in <-chan interface{}) {
	for elem := range in {
		queue.Push(elem)
		select {
		case <-side:
		default:
		}
	}
	for {
		select {
		case side <- shutdown:
			break
		case <-side:
		}
	}
}

func writeOut(queue infqueue.Queue, side chan msg, out chan<- interface{}) {
	shuttingDown := false
	for {
		select {
		case <-side:
			shuttingDown = true
		default:
			elem := queue.Shift()
			if elem == nil {
				if shuttingDown {
					break
				}
				side <- starving
				continue
			}
			out <- elem
		}
	}
}
