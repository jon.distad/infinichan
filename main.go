package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"runtime/pprof"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/catharinejm/infinichan/config"
	"gitlab.com/catharinejm/infinichan/infqueue"
	"gitlab.com/catharinejm/infinichan/utils"
)

func main() {
	conf := config.Base()
	fmt.Println("GOMAXPROCS:", runtime.GOMAXPROCS(0))
	fmt.Println("SPINCAP:", infqueue.SPINCAP())

	if conf.RunTrace && conf.ThreadCount > 1000000 {
		fmt.Println("Running trace with more than 1,000,000 threads will probably crash your computer.")
		os.Exit(1)
	}

	if conf.CPUProfile != "" {
		fmt.Println("**** cpuprofile enabled")
		fmt.Println("---- writing to", conf.CPUProfile)
		f, err := os.Create(conf.CPUProfile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		go func() {
			<-c
			pprof.StopCPUProfile()
			os.Exit(1)
		}()
		defer pprof.StopCPUProfile()
	}

	elemsPerThread := (conf.ElemCount + (conf.ThreadCount - 1)) / conf.ThreadCount
	totalElements := elemsPerThread * conf.ThreadCount
	fmt.Printf("Running with %v goroutines and %v elements (%v per goroutine)\n",
		conf.ThreadCount, totalElements, elemsPerThread)

	buf := infqueue.NewQueue().(infqueue.QueueTrace)

	ticker := time.NewTicker(10 * time.Millisecond)
	final := make(chan struct{})
	go func() {
		msg := "Stores: 0, Loads: 0"
		fmt.Printf(msg)
		isFinal := false
		for {
			select {
			case <-ticker.C:
			case <-final:
				isFinal = true
			}
			stores := atomic.LoadInt32(&storeCount)
			loads := atomic.LoadInt32(&loadCount)
			deleteLen := len(msg)
			msg = fmt.Sprintf("Stores: %v, Loads: %v", stores, loads)
			fmt.Printf("%s", strings.Repeat("\b", deleteLen))
			fmt.Printf("%s", msg)
			if isFinal {
				fmt.Printf("\n")
				break
			}
		}
		final <- struct{}{}
	}()

	if conf.RunTrace {
		result := storeParallelTrace(buf, conf.ThreadCount, elemsPerThread)
		fmt.Printf("\nStore trace info: %#v\n", result)
	} else {
		storeParallel(buf, conf.ThreadCount, elemsPerThread)
	}
	// storeSerial(buf, loopMax)

	// loadParallel(buf, loopMax, false)
	// loadSerial(buf, loopMax)
	ticker.Stop()
	final <- struct{}{}
	<-final

	time.Now()
}

var (
	storeCount int32 = 0
	loadCount  int32 = 0
)

func storeParallel(buf infqueue.Queue, threads, elemsPerThread int) {
	wg := sync.WaitGroup{}
	for i := 0; i < threads; i++ {
		wg.Add(1)
		go func(i int) {
			defer func() {
				wg.Done()
				atomic.AddInt32(&storeCount, int32(elemsPerThread))
			}()
			for j := 0; j < elemsPerThread; j++ {
				buf.Push(i*threads + j)
			}
		}(i)
	}
	wg.Wait()
}

func storeParallelTrace(buf infqueue.QueueTrace, threads, elemsPerThread int) *infqueue.TraceInfo {
	wg := sync.WaitGroup{}
	info := &infqueue.TraceInfo{}
	infoChan := make(chan *infqueue.TraceInfo, utils.MinInt((threads+99)/100, 1000))
	wg.Add(1)
	go func() {
		count := 0
	loop:
		for {
			select {
			case i := <-infoChan:
				info.Add(i)
				count++
				if count == threads {
					break loop
				}
			}
		}
		wg.Done()
	}()

	ctx, cancel := context.WithCancel(context.Background())

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	go func() {
		<-sigChan
		cancel()
	}()

	for i := 0; i < threads; i++ {
		go func(i int) {
			defer func() {
				atomic.AddInt32(&storeCount, int32(elemsPerThread))
			}()
			info := &infqueue.TraceInfo{}
			for j := 0; j < elemsPerThread; j++ {
				info.Add(buf.PushTrace(ctx, i*threads+j))
			}
			infoChan <- info
		}(i)
	}
	wg.Wait()
	return info
}

func loadParallel(buf infqueue.Queue, threads int, keep bool) []int {
	wg := sync.WaitGroup{}
	var result []int
	if keep {
		result = make([]int, threads)
	}
	for i := 0; i < threads; i++ {
		wg.Add(1)
		go func(i int) {
			defer func() {
				wg.Done()
				atomic.AddInt32(&loadCount, 1)
			}()
			elem := buf.Shift()
			if keep {
				if elem == nil {
					result[i] = -1
				} else {
					result[i] = elem.(int)
				}
			}
		}(i)
	}
	wg.Wait()
	return result
}

func storeSerial(buf infqueue.Queue, threads int) {
	for i := 0; i < threads; i++ {
		buf.Push(i)
		storeCount++
	}
}

func loadSerial(buf infqueue.Queue, threads int) {
	for i := 0; i < threads; i++ {
		buf.Shift()
		loadCount++
	}
}
