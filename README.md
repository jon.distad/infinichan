### Notes

I originally wanted to implement an unbounded channel, but then I got
more interested in implementing an unbounded, lock-free queue for that
channel to use as a backing store. I can't guarantee that any of this
is implemented correctly, but it seems OK so far...
